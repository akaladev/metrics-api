package com.metrics.base.model;

/**
 * Created by akalamichael on 4/11/17.
 */
public class Status {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
