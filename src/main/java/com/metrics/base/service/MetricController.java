package com.metrics.base.service;


import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.metrics.base.model.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import jdepend.framework.JDepend;
import jdepend.framework.JavaClass;
import jdepend.framework.JavaPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import org.hibernate.Hibernate;
import java.util.*;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Api(value = "metrics", basePath = "/metrics",  produces = "application/json")

@RestController
public class MetricController {


    /**
     * POST /uploadFile -> receive and locally save a file.
     *
     * @param uploadfile The uploaded file as Multipart file parameter in the
     * HTTP request. The RequestParam name must be the same of the attribute
     * "name" in the input tag with type file.
     *
     * @return An http OK status in case of success, an http 4xx status in case
     * of errors.
     */

    @ApiOperation(value = "uploading jar file")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "") })
    @RequestMapping(value = "/metrics/uploadJar", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> uploadFile(
            @RequestParam("uploadfile") MultipartFile uploadfile) {

        try {
            // Get the filename and build the local file path
            String filename = uploadfile.getOriginalFilename();
            String directory = System.getProperty("user.home");
            String filepath = Paths.get(directory, filename).toString();

            // Save the file locally
            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(filepath)));
            stream.write(uploadfile.getBytes());
            stream.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    } // method uploadFile


    @ApiOperation(value = "analyze a given jar file.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = ""),
            @ApiResponse(code = 404, message = "If the task with this id, doesn't exist")
    })
    @RequestMapping(value = "/analyze/", method = GET, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,List<JavaClass>>> analyze(@RequestParam("id") String id) {
        JDepend jdepend = null;
        System.out.println(id);
        Map<String,List<JavaClass>> map = new HashMap<String, List<JavaClass>>();
        try {
            jdepend = new JDepend();
            String userHome = System.getProperty("user.home")+"/";
            jdepend.addDirectory(userHome + id +".jar");
            Collection packages = jdepend.analyze();
            Iterator iter = packages.iterator();
            List<JavaClass> list = null;
            while(iter.hasNext()){
                JavaPackage p = (JavaPackage) iter.next();
                System.out.println(p.getClassCount());
                Collection classes = p.getClasses();
                Iterator classIter = classes.iterator();
                while(classIter.hasNext()){
                    list = new ArrayList<JavaClass>();
                    JavaClass c = (JavaClass) classIter.next();
                    list.add(c);
                }
                map.put(p.getName(), list);
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Map<String,List<JavaClass>>>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Map<String,List<JavaClass>>>(map, OK);

    }

}


